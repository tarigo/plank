#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"

#include "plank/entry.hpp"
#include "plank/entry_fwd.hpp"
#include "plank/logger.hpp"
#include "plank/logger_fwd.hpp"
#include "plank/writer/console.hpp"

#include <iostream>
#include <memory>

using namespace plank;

namespace  {

const auto TAG = "UNIT_TEST";

std::string to_str(enum log::level level) {
    switch (level) {
    case log::level::critical:  return "critical";
    case log::level::error:     return "error";
    case log::level::warning:   return "warning";
    case log::level::info:      return "info";
    case log::level::debug:     return "debug";
    case log::level::verbose:   return "verbose";
    default:                    return "WTF";
    }
}

}


TEST_CASE("Write to console") {
    auto writer = new log::writer::console<log::entry>{};
    auto writer_ptr =
            std::unique_ptr<log::writer::basic_interface<log::entry>>(writer);
    auto console_logger = log::logger(std::move(writer_ptr));

    auto levels = { log::level::critical,
                    log::level::error,
                    log::level::warning,
                    log::level::info,
                    log::level::debug,
                    log::level::verbose };

    for (const auto level : levels) {
        REQUIRE_NOTHROW(console_logger << (level << TAG
                           << "Test " << to_str(level) << " message."));
    }
}
