#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"

#include <string>
#include <iostream>

#include "plank/entry.hpp"
#include "plank/entry_fwd.hpp"

using namespace plank;

namespace  {

const auto TAG = "unit-test";

std::string to_string(enum log::level level) {
    switch (level) {
    case log::level::critical:  return "CRITICAL";
    case log::level::error:     return "ERROR";
    case log::level::warning:   return "WARNING";
    case log::level::info:      return "INFO";
    case log::level::debug:     return "DEBUG";
    case log::level::verbose:   return "VERBOSE";
    default:                    return "N/A";
    }
}

}

TEST_CASE("Create and populate entry") {
    const auto message = "Bla-bla-bla...";

    auto levels = { log::level::critical,
                    log::level::error,
                    log::level::warning,
                    log::level::info,
                    log::level::debug,
                    log::level::verbose };

    for (const auto level : levels) {
        auto e = log::entry{ level, TAG };
        auto buffer = to_string(level) + " " + message;
        e.buffer << message;

        REQUIRE(e.level == level);
        REQUIRE(e.tag.name == TAG);
        REQUIRE(e.buffer.str() == message);
    }
}
