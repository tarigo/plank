#pragma once

// #include "plank/tag.hpp"


#include <string>
#include <ostream>

namespace plank::log {

struct tag {
    std::string name;

    tag() : name("") {}
    explicit tag(const std::string &n) : name{n} {}
    explicit tag(std::string &&n) noexcept: name{std::move(n)} {}
    explicit tag(const char *n) : name{n} {}

    tag(const tag& other) = default;
    tag(tag&& other) = default;

    ~tag() = default;

    bool operator == (const tag& rhs) const noexcept {
        return name == rhs.name;
    }
};

inline std::ostream & operator << (std::ostream &os, const tag &tag) {
    os << tag.name;
    return os;
}

}

// #include "plank/logger.hpp"


// #include "plank/entry.hpp"


// #include "plank/level.hpp"


#include <cstdint>

namespace plank::log {

enum class level : uint8_t {
    critical,
    error,
    warning,
    info,
    debug,
    verbose
};

}

// #include "plank/tag.hpp"


#include <memory>
#include <thread>
#include <optional>

namespace plank::log {

template<typename Timestamp, typename Buffer>
struct basic_entry {
    using timestamp_type = Timestamp;
    using buffer_type = Buffer;

    enum level level;
    struct tag tag;
    Buffer buffer{};
    std::optional<Timestamp> timestamp{};
    std::optional<std::thread::id> thread_id{};

    template <typename Tag = struct tag>
    basic_entry(enum level l, Tag &&t) noexcept:
        level{l}, tag{std::forward<Tag>(t)} {}

    template <typename Tag = struct tag>
    explicit basic_entry(Tag &&t) noexcept:
        level(level::verbose), tag{std::forward<Tag>(t)} {}

    basic_entry(basic_entry&& other) = default;

    virtual ~basic_entry() = default;

    basic_entry& operator =(basic_entry&& other) = default;
};

namespace {

template <typename T, typename = int>
struct has_level : std::false_type {};
template <typename T>
struct has_level <T, decltype((void) T::level, 0)> : std::true_type {};

template <typename T, typename = int>
struct has_tag : std::false_type {};
template <typename T>
struct has_tag <T, decltype((void) T::tag, 0)> : std::true_type {};

template <typename T, typename = int>
struct has_buffer : std::false_type {};
template <typename T>
struct has_buffer <T, decltype((void) T::buffer, 0)> : std::true_type {};

}

template <typename T, typename = int>
struct is_entry : std::false_type {};
template <typename T>
struct is_entry<T> : std::false_type {
    static constexpr auto value = has_level<T>::value and
                                  has_tag<T>::value and
                                  has_buffer<T>::value;
};

template <typename Entry, typename Message,
          typename = std::enable_if_t<is_entry<Entry>::value>>
Entry operator << (Entry &&entry, Message &&message) {
   entry.buffer << std::forward<Message>(message);
   return std::forward<Entry>(entry);
}

}

// #include "plank/option.hpp"


#include <cstdint>

namespace plank::log {

namespace option {

using flags = uint32_t;

constexpr flags none = 0,
                show_timestamp = 1 << 1,
                show_level = 1 << 2,
                show_tag = 1 << 3,
                show_pid = 1 << 4,
                show_tid = 1 << 5,
                colored = 1 << 6;

#ifdef __ANDROID__
constexpr flags defaults = show_tag;
#else
constexpr flags defaults = show_timestamp | show_level | show_tag | show_tid;
#endif


}

}

// #include "plank/writer/basic_interface.hpp"


// #include "plank/entry.hpp"

// #include "plank/option.hpp"


namespace plank::log::writer {

template <typename Entry>
struct basic_interface {
    virtual ~basic_interface() = default;

    virtual const std::string& name() const noexcept = 0;
    virtual bool is_thread_safe() const noexcept = 0;

    virtual void write(const Entry &entry, option::flags flags) = 0;
    virtual void write(Entry &&entry, option::flags flags) = 0;
};

}


#include <memory>
#include <utility>
#include <mutex>
#include <atomic>

namespace plank::log {

template <typename Entry,
          typename Entry::timestamp_type (*TimeNow)()>
class basic_logger {
public:
    using entry_type = Entry;
    using writer_ptr = std::unique_ptr<writer::basic_interface<entry_type>>;

    virtual ~basic_logger() = default;

    explicit
    basic_logger(writer_ptr &&writer) noexcept: writer_{std::move(writer)} {
        if (writer_ != nullptr) {
            is_writer_thread_safe_ = writer_->is_thread_safe();
        }
    }

    template<typename E = entry_type>
    void operator << (E&& e) {
        write(std::forward<E>(e));
    }

    entry_type operator << (level l) const {
        return {l};
    }

    level max_level() const {
        return max_level_;
    }

    void max_level(level l) {
        max_level_ = l;
    }

    option::flags option_flags() const {
        return option_flags_;
    }

    void option_flags(option::flags flags) {
        option_flags_ = flags;
    }

    const writer::basic_interface<entry_type>& writer() const {
        writer_.get();
    }

    void writer(writer_ptr writer) {
        std::lock_guard<decltype(writer_mutex_)> lock(writer_mutex_);
        writer_.reset(writer.release());
        if (writer_ != nullptr) {
            is_writer_thread_safe_ = writer_->is_thread_safe();
        }
    }

public:
    template <typename E = entry_type>
    void write(E && e) {
        if (e.level > max_level_) return;

        if (option_flags_ & option::show_timestamp)
            e.timestamp = TimeNow();
        if (option_flags_ & option::show_tid)
            e.thread_id = std::this_thread::get_id();

        if (!writer_) return;

        if (is_writer_thread_safe_.load()) {
            writer_->write(std::forward<E>(e), option_flags_);
        } else {
            std::lock_guard<decltype(writer_mutex_)> lock(writer_mutex_);
            writer_->write(std::forward<E>(e), option_flags_);
        }
    }

private:
    option::flags option_flags_ = option::defaults;

    std::atomic<level> max_level_{level::verbose};
    std::mutex writer_mutex_{};
    writer_ptr writer_ = nullptr;
    std::atomic_bool is_writer_thread_safe_{true};
};

}

// #include "plank/log_fwd.hpp"


// #include "plank/entry_fwd.hpp"


#include <chrono>
#include <sstream>

namespace plank::log {

using entry = basic_entry<std::chrono::system_clock::time_point,
                          std::stringstream>;

}

// #include "plank/logger_fwd.hpp"


// #include "plank/entry_fwd.hpp"


#include <chrono>
#include <utility>

namespace plank::log {

using logger = basic_logger<entry, std::chrono::system_clock::now>;

template <typename Tag = tag>
inline auto operator << (enum level level,  Tag &&tag) {
    return logger::entry_type{ level, std::forward<Tag>(tag) };
}

inline auto operator << (enum level level, const char *tag) {
    return logger::entry_type{ level, tag };
}

template <typename Message>
auto operator << (const tag &tag, Message &&message) {
   auto e = logger::entry_type{ tag };
   e.buffer << std::forward<Message>(message);
   return e;
}

}


// #include "plank/writer/writer.hpp"


// #include "plank/writer/basic_interface.hpp"

// #include "plank/writer/console.hpp"


// #include "plank/writer/basic_interface.hpp"

// #include "plank/option.hpp"

// #include "plank/entry.hpp"


#include <sstream>
#include <iostream>
#include <stdexcept>

namespace plank::log::writer {

inline std::string to_string(enum log::level level) {
    switch (level) {
    case log::level::critical:  return "[C]";
    case log::level::error:     return "[E]";
    case log::level::warning:   return "[W]";
    case log::level::info:      return "[I]";
    case log::level::debug:     return "[D]";
    case log::level::verbose:   return "[V]";
    default:                    return "[N/A]";
    }
}

template <typename Entry>
class console : public basic_interface<Entry> {
public:
    virtual const std::string& name() const noexcept override {
        return name_;
    }

    virtual bool is_thread_safe() const noexcept override { return false; }

    virtual void write(const Entry& entry, option::flags flags) override {
        std::stringstream oss;

        if (flags & option::show_timestamp && entry.timestamp) {
            auto t = entry.timestamp.value().time_since_epoch();
            auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(t);
            oss << ms.count() << " ";
        }
        if (flags & option::show_level) {
            oss << to_string(entry.level) << " ";
        }
        if (flags & option::show_tag) {
            oss << "(" << entry.tag << ") ";
        }
        if (flags & option::show_tid && entry.thread_id) {
            oss << "{" << *entry.thread_id << "} ";
        }

        switch (entry.level) {
        case level::critical:
        case level::error:
            std::cerr << oss.rdbuf() << entry.buffer.rdbuf()  << "\n";
            break;
        case level::verbose:
        case level::debug:
        case level::info:
        case level::warning:
            std::cout << oss.rdbuf() << entry.buffer.rdbuf()  << '\n';
            break;
        default:
            ;
        }
    }

    virtual void write(Entry&& entry, option::flags flags) override {
        write(entry, flags);
    }

private:
    const std::string name_{"console"};
};

}

// #include "plank/writer/android.hpp"

#ifdef __ANDROID__

// #include "plank/writer/basic_interface.hpp"

#include "plank/log/option.hpp"
#include "plank/log/entry.hpp"

#include <sstream>
#include <iostream>
#include <stdexcept>

#include <android/log.h>

namespace plank::log::writer {

template <typename Entry>
class android : public basic_interface<Entry> {
public:
    virtual const std::string& name() const noexcept override {
        return name_;
    }

    virtual bool is_thread_safe() const noexcept override {
        return true;
    }

    virtual void write(const Entry& entry, option::flags flags) override {
        if (flags) {}

        int priority = ANDROID_LOG_UNKNOWN;
        switch (entry.level) {
        case level::verbose:
            priority = ANDROID_LOG_VERBOSE;
            break;
        case level::debug:
            priority = ANDROID_LOG_DEBUG;
            break;
        case level::info:
            priority = ANDROID_LOG_INFO;
            break;
        case level::warning:
            priority = ANDROID_LOG_WARN;
            break;
        case level::error:
            priority = ANDROID_LOG_ERROR;
            break;
        case level::critical:
            priority = ANDROID_LOG_FATAL;
            break;
        }
        __android_log_write(priority,
                            entry.tag.name.c_str(),
                            entry.buffer.str().c_str());
    }

    virtual void write(Entry&& entry, option::flags flags) override {
        write(entry, flags);
    }

private:
    const std::string name_{"android"};
};

}
#endif //__ANDROID__



namespace plank::log {

template <typename Logger = logger,
#ifdef __ANDROID__
          typename Writer = writer::android<typename Logger::entry_type>>
#else
          typename Writer = writer::console<typename Logger::entry_type>>
#endif
static Logger& default_logger() {
    static auto instance = Logger{ typename Logger::writer_ptr{new Writer{}} };
    return instance;
}

template <typename Logger = logger>
void get_max_level(Logger &logger = default_logger()) {
    return logger.max_level();
}

template <typename Logger = logger>
void set_max_level(level level, Logger &logger = default_logger()) {
    return logger.max_level(level);
}

template <typename Logger = logger>
void critical(typename Logger::entry_type &&e, Logger &l = default_logger()) {
    e.level = level::critical;
    l << std::forward<typename Logger::entry_type>(e);
}

template <typename Logger = logger>
void error(typename Logger::entry_type &&e, Logger &l = default_logger()) {
    e.level = level::error;
    l << std::forward<typename Logger::entry_type>(e);
}

template <typename Logger = logger>
void warning(typename Logger::entry_type &&e, Logger &l = default_logger()) {
    e.level = level::warning;
    l << std::forward<typename Logger::entry_type>(e);
}

template <typename Logger = logger>
void info(typename Logger::entry_type &&e, Logger &l = default_logger()) {
    e.level = level::info;
    l << std::forward<typename Logger::entry_type>(e);
}

template <typename Logger = logger>
void debug(typename Logger::entry_type &&e, Logger &l = default_logger()) {
    e.level = level::debug;
    l << std::forward<typename Logger::entry_type>(e);
}

template <typename Logger = logger>
void verbose(typename Logger::entry_type &&e, Logger &l = default_logger()) {
    e.level = level::verbose;
    l << std::forward<typename Logger::entry_type>(e);
}

} // namespace plank::log
