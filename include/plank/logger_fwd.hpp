#pragma once

#include "plank/entry_fwd.hpp"

#include <chrono>
#include <utility>

namespace plank::log {

using logger = basic_logger<entry, std::chrono::system_clock::now>;

template <typename Tag = tag>
inline auto operator << (enum level level,  Tag &&tag) {
    return logger::entry_type{ level, std::forward<Tag>(tag) };
}

inline auto operator << (enum level level, const char *tag) {
    return logger::entry_type{ level, tag };
}

template <typename Message>
auto operator << (const tag &tag, Message &&message) {
   auto e = logger::entry_type{ tag };
   e.buffer << std::forward<Message>(message);
   return e;
}

}
