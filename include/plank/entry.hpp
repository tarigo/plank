#pragma once

#include "plank/level.hpp"
#include "plank/tag.hpp"

#include <memory>
#include <thread>
#include <optional>

namespace plank::log {

template<typename Timestamp, typename Buffer>
struct basic_entry {
    using timestamp_type = Timestamp;
    using buffer_type = Buffer;

    enum level level;
    struct tag tag;
    Buffer buffer{};
    std::optional<Timestamp> timestamp{};
    std::optional<std::thread::id> thread_id{};

    template <typename Tag = struct tag>
    basic_entry(enum level l, Tag &&t) noexcept:
        level{l}, tag{std::forward<Tag>(t)} {}

    template <typename Tag = struct tag>
    explicit basic_entry(Tag &&t) noexcept:
        level(level::verbose), tag{std::forward<Tag>(t)} {}

    basic_entry(basic_entry&& other) = default;

    virtual ~basic_entry() = default;

    basic_entry& operator =(basic_entry&& other) = default;
};

namespace {

template <typename T, typename = int>
struct has_level : std::false_type {};
template <typename T>
struct has_level <T, decltype((void) T::level, 0)> : std::true_type {};

template <typename T, typename = int>
struct has_tag : std::false_type {};
template <typename T>
struct has_tag <T, decltype((void) T::tag, 0)> : std::true_type {};

template <typename T, typename = int>
struct has_buffer : std::false_type {};
template <typename T>
struct has_buffer <T, decltype((void) T::buffer, 0)> : std::true_type {};

}

template <typename T, typename = int>
struct is_entry : std::false_type {};
template <typename T>
struct is_entry<T> : std::false_type {
    static constexpr auto value = has_level<T>::value and
                                  has_tag<T>::value and
                                  has_buffer<T>::value;
};

template <typename Entry, typename Message,
          typename = std::enable_if_t<is_entry<Entry>::value>>
Entry operator << (Entry &&entry, Message &&message) {
   entry.buffer << std::forward<Message>(message);
   return std::forward<Entry>(entry);
}

}
