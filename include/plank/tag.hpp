#pragma once

#include <string>
#include <ostream>

namespace plank::log {

struct tag {
    std::string name;

    tag() : name("") {}
    explicit tag(const std::string &n) : name{n} {}
    explicit tag(std::string &&n) noexcept: name{std::move(n)} {}
    explicit tag(const char *n) : name{n} {}

    tag(const tag& other) = default;
    tag(tag&& other) = default;

    ~tag() = default;

    bool operator == (const tag& rhs) const noexcept {
        return name == rhs.name;
    }
};

inline std::ostream & operator << (std::ostream &os, const tag &tag) {
    os << tag.name;
    return os;
}

}
