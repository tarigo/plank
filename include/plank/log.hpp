#pragma once

#include "plank/tag.hpp"
#include "plank/logger.hpp"
#include "plank/log_fwd.hpp"
#include "plank/writer/writer.hpp"

namespace plank::log {

template <typename Logger = logger,
#ifdef __ANDROID__
          typename Writer = writer::android<typename Logger::entry_type>>
#else
          typename Writer = writer::console<typename Logger::entry_type>>
#endif
static Logger& default_logger() {
    static auto instance = Logger{ typename Logger::writer_ptr{new Writer{}} };
    return instance;
}

template <typename Logger = logger>
void get_max_level(Logger &logger = default_logger()) {
    return logger.max_level();
}

template <typename Logger = logger>
void set_max_level(level level, Logger &logger = default_logger()) {
    return logger.max_level(level);
}

template <typename Logger = logger>
void critical(typename Logger::entry_type &&e, Logger &l = default_logger()) {
    e.level = level::critical;
    l << std::forward<typename Logger::entry_type>(e);
}

template <typename Logger = logger>
void error(typename Logger::entry_type &&e, Logger &l = default_logger()) {
    e.level = level::error;
    l << std::forward<typename Logger::entry_type>(e);
}

template <typename Logger = logger>
void warning(typename Logger::entry_type &&e, Logger &l = default_logger()) {
    e.level = level::warning;
    l << std::forward<typename Logger::entry_type>(e);
}

template <typename Logger = logger>
void info(typename Logger::entry_type &&e, Logger &l = default_logger()) {
    e.level = level::info;
    l << std::forward<typename Logger::entry_type>(e);
}

template <typename Logger = logger>
void debug(typename Logger::entry_type &&e, Logger &l = default_logger()) {
    e.level = level::debug;
    l << std::forward<typename Logger::entry_type>(e);
}

template <typename Logger = logger>
void verbose(typename Logger::entry_type &&e, Logger &l = default_logger()) {
    e.level = level::verbose;
    l << std::forward<typename Logger::entry_type>(e);
}

} // namespace plank::log
