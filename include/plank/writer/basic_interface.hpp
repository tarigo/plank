#pragma once

#include "plank/entry.hpp"
#include "plank/option.hpp"

namespace plank::log::writer {

template <typename Entry>
struct basic_interface {
    virtual ~basic_interface() = default;

    virtual const std::string& name() const noexcept = 0;
    virtual bool is_thread_safe() const noexcept = 0;

    virtual void write(const Entry &entry, option::flags flags) = 0;
    virtual void write(Entry &&entry, option::flags flags) = 0;
};

}
