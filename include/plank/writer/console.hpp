#pragma once

#include "plank/writer/basic_interface.hpp"
#include "plank/option.hpp"
#include "plank/entry.hpp"

#include <sstream>
#include <iostream>
#include <stdexcept>

namespace plank::log::writer {

inline std::string to_string(enum log::level level) {
    switch (level) {
    case log::level::critical:  return "[C]";
    case log::level::error:     return "[E]";
    case log::level::warning:   return "[W]";
    case log::level::info:      return "[I]";
    case log::level::debug:     return "[D]";
    case log::level::verbose:   return "[V]";
    default:                    return "[N/A]";
    }
}

template <typename Entry>
class console : public basic_interface<Entry> {
public:
    virtual const std::string& name() const noexcept override {
        return name_;
    }

    virtual bool is_thread_safe() const noexcept override { return false; }

    virtual void write(const Entry& entry, option::flags flags) override {
        std::stringstream oss;

        if (flags & option::show_timestamp && entry.timestamp) {
            auto t = entry.timestamp.value().time_since_epoch();
            auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(t);
            oss << ms.count() << " ";
        }
        if (flags & option::show_level) {
            oss << to_string(entry.level) << " ";
        }
        if (flags & option::show_tag) {
            oss << "(" << entry.tag << ") ";
        }
        if (flags & option::show_tid && entry.thread_id) {
            oss << "{" << *entry.thread_id << "} ";
        }

        switch (entry.level) {
        case level::critical:
        case level::error:
            std::cerr << oss.rdbuf() << entry.buffer.rdbuf()  << "\n";
            break;
        case level::verbose:
        case level::debug:
        case level::info:
        case level::warning:
            std::cout << oss.rdbuf() << entry.buffer.rdbuf()  << '\n';
            break;
        default:
            ;
        }
    }

    virtual void write(Entry&& entry, option::flags flags) override {
        write(entry, flags);
    }

private:
    const std::string name_{"console"};
};

}
