#pragma once
#ifdef __ANDROID__

#include "plank/writer/basic_interface.hpp"
#include "plank/log/option.hpp"
#include "plank/log/entry.hpp"

#include <sstream>
#include <iostream>
#include <stdexcept>

#include <android/log.h>

namespace plank::log::writer {

template <typename Entry>
class android : public basic_interface<Entry> {
public:
    virtual const std::string& name() const noexcept override {
        return name_;
    }

    virtual bool is_thread_safe() const noexcept override {
        return true;
    }

    virtual void write(const Entry& entry, option::flags flags) override {
        if (flags) {}

        int priority = ANDROID_LOG_UNKNOWN;
        switch (entry.level) {
        case level::verbose:
            priority = ANDROID_LOG_VERBOSE;
            break;
        case level::debug:
            priority = ANDROID_LOG_DEBUG;
            break;
        case level::info:
            priority = ANDROID_LOG_INFO;
            break;
        case level::warning:
            priority = ANDROID_LOG_WARN;
            break;
        case level::error:
            priority = ANDROID_LOG_ERROR;
            break;
        case level::critical:
            priority = ANDROID_LOG_FATAL;
            break;
        }
        __android_log_write(priority,
                            entry.tag.name.c_str(),
                            entry.buffer.str().c_str());
    }

    virtual void write(Entry&& entry, option::flags flags) override {
        write(entry, flags);
    }

private:
    const std::string name_{"android"};
};

}
#endif //__ANDROID__
