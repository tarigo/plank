#pragma once

#include "plank/entry.hpp"
#include "plank/option.hpp"
#include "plank/writer/basic_interface.hpp"

#include <memory>
#include <utility>
#include <mutex>
#include <atomic>

namespace plank::log {

template <typename Entry,
          typename Entry::timestamp_type (*TimeNow)()>
class basic_logger {
public:
    using entry_type = Entry;
    using writer_ptr = std::unique_ptr<writer::basic_interface<entry_type>>;

    virtual ~basic_logger() = default;

    explicit
    basic_logger(writer_ptr &&writer) noexcept: writer_{std::move(writer)} {
        if (writer_ != nullptr) {
            is_writer_thread_safe_ = writer_->is_thread_safe();
        }
    }

    template<typename E = entry_type>
    void operator << (E&& e) {
        write(std::forward<E>(e));
    }

    entry_type operator << (level l) const {
        return {l};
    }

    level max_level() const {
        return max_level_;
    }

    void max_level(level l) {
        max_level_ = l;
    }

    option::flags option_flags() const {
        return option_flags_;
    }

    void option_flags(option::flags flags) {
        option_flags_ = flags;
    }

    const writer::basic_interface<entry_type>& writer() const {
        writer_.get();
    }

    void writer(writer_ptr writer) {
        std::lock_guard<decltype(writer_mutex_)> lock(writer_mutex_);
        writer_.reset(writer.release());
        if (writer_ != nullptr) {
            is_writer_thread_safe_ = writer_->is_thread_safe();
        }
    }

public:
    template <typename E = entry_type>
    void write(E && e) {
        if (e.level > max_level_) return;

        if (option_flags_ & option::show_timestamp)
            e.timestamp = TimeNow();
        if (option_flags_ & option::show_tid)
            e.thread_id = std::this_thread::get_id();

        if (!writer_) return;

        if (is_writer_thread_safe_.load()) {
            writer_->write(std::forward<E>(e), option_flags_);
        } else {
            std::lock_guard<decltype(writer_mutex_)> lock(writer_mutex_);
            writer_->write(std::forward<E>(e), option_flags_);
        }
    }

private:
    option::flags option_flags_ = option::defaults;

    std::atomic<level> max_level_{level::verbose};
    std::mutex writer_mutex_{};
    writer_ptr writer_ = nullptr;
    std::atomic_bool is_writer_thread_safe_{true};
};

}
