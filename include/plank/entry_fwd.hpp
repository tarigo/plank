#pragma once

#include <chrono>
#include <sstream>

namespace plank::log {

using entry = basic_entry<std::chrono::system_clock::time_point,
                          std::stringstream>;

}
