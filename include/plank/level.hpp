#pragma once

#include <cstdint>

namespace plank::log {

enum class level : uint8_t {
    critical,
    error,
    warning,
    info,
    debug,
    verbose
};

}
