#pragma once

#include <cstdint>

namespace plank::log {

namespace option {

using flags = uint32_t;

constexpr flags none = 0,
                show_timestamp = 1 << 1,
                show_level = 1 << 2,
                show_tag = 1 << 3,
                show_pid = 1 << 4,
                show_tid = 1 << 5,
                colored = 1 << 6;

#ifdef __ANDROID__
constexpr flags defaults = show_tag;
#else
constexpr flags defaults = show_timestamp | show_level | show_tag | show_tid;
#endif


}

}
