#!/bin/sh

cppcheck --error-exitcode=1 --verbose --language=c++ --std=c++11 \
	 --enable=all --inconclusive \
	 -I include --suppress=missingIncludeSystem \
	 --suppressions-list=./scripts/cppcheck/suppressions.txt \
	 test/src/cppcheck/main.cpp

